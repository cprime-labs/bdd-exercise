# bdd-exercise

## Getting started

The functional requirements of this exercise are described in the '[account.feature](./account.feature)' feature file in the root directory of this repository.

The code required to begin this exercise is available as a project template. To start the exercise, select 'new project' then 'from template' and select the bdd-exercise template from the list of available options. 

## Bank of APIs

This exercise requires you to write a simple application to call the NatWest [Bank of APIs](https://www.bankofapis.com/).

For detailed information on access to the Bank of APIs, read the [guide](https://www.bankofapis.com/articles/restricted-api-doc-access) provided by the Bank of APIs product team.

## Behavior Driven Development (BDD)

If BDD is an unfamiliar concept to you, an excellent introduction to BDD is available here on [cucumber.io](https://cucumber.io/docs/bdd/).

Depending upon which programming language or framework you have selected to complete this exercise, we recommend the following BDD libraries to execute the BDD scenarios. This is only a recommendation; feel free to implement BDD tests however you consider most appropriate.

[Java:  Cucumber-JVM](https://cucumber.io/docs/installation/java/)

[Node JS: Cucumber.js](https://cucumber.io/docs/installation/javascript/)

[Python: Behave](https://behave.readthedocs.io/en/latest/)

## Assessment Criteria

### Scope Complete

We will measure scope complete as the percentage of the BDD scenarios successfully implemented within your code base.

### Code Quality

We will measure code quality utilizing GitLab CICD code quality analysis, which implements the open-source Code Climate tool and selected plugins.

### Code Security

We will measure code security utilizing GitLab CICD SAST analyzers. For all languages and frameworks involved in this exercise, the underlying tool executed by GitLab SAST analysis will be Semgrep with GitLab-managed rules.

### Developer Feedback

After completing the exercise, we ask you to complete the [Developer Feedback](#) survey. This survey will help us gather valuable insight from engineers like yourself to inform us how AI-assisted features may support our developer community.
