Feature: Review Customer Account Details

  As a consumer of the NatWest Open Banking APIs
  I want to view account information for a specific customer's account
  So that I can analyze that customer's account usage.

  Acceptance Criteria
  - All account information relates to a single customer
  - I can view all balances relating to that customer's account
  - I can view all beneficiaries relating to that customer's account
  - I can view all direct debits relating to that customer's account
  - I can view all offers relating to that customer's account


  Scenario: Retrieve balances for a specific NatWest customer account
    Given an example scenario
    When all step definitions are implemented
    Then the scenario passes

  Scenario: Retrieve beneficiaries for a specific NatWest customer account
    Given an example scenario
    When all step definitions are implemented
    Then the scenario passes

  Scenario: Retrieve direct debits for a specific NatWest customer account
    Given an example scenario
    When all step definitions are implemented
    Then the scenario passes

  Scenario: Retrieve offers for a specific NatWest customer account
    Given an example scenario
    When all step definitions are implemented
    Then the scenario passes
